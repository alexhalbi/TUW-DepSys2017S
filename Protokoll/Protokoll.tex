\NeedsTeXFormat{LaTeX2e}
\documentclass[10pt,a4paper,titlepage]{article}
\usepackage[ngerman]{babel} % german text
\usepackage[DIV=12]{typearea} % size of printable area
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx} % to include images
\usepackage{tikz}
\usetikzlibrary{arrows,automata,matrix,patterns}

 \tikzset{
endbox/.style={pattern=crosshatch,minimum height=.8cm}}
\usepackage[colorlinks=true]{hyperref}
\setcounter{tocdepth}{1}
\setlength{\parindent}{0pt}
\usepackage{array}
\usepackage{tabularx}
\usepackage{float}
\usepackage{caption}
\usepackage{graphicx} % to include images
\usepackage{subfigure} % for creating subfigures
\usepackage{bm} %Bold Math
\usepackage[miktex]{gnuplottex}
\newcommand\tab[1][1cm]{\hspace*{#1}}
\renewcommand{\familydefault}{\sfdefault} % activate to use sans-serif font as default

\sloppy % friendly typesetting
\begin{document}

\begin{titlepage}

\begin{figure*}[h!]
  \includegraphics[width=8cm]{TULogo_CMYK}
\end{figure*}

\begin{center}
\vspace*{1.3cm}
{\Huge Dependable Systems\\(VU  182.712)\\}
\vspace{1.7cm}
{\LARGE Praktisches Übungsbeispiel SS2017\\}
\vspace{5cm}

\begin{table}[h!]
\centering
\begin{tabular}{l|l|l|l}
\textbf{Matr. Nr.} & \textbf{Kennzahl} & \textbf{Name} & \textbf{E-Mail}\\
\hline
1129193 & 033 535 & Alexander Halbarth & e1129193@student.tuwien.ac.at\\
1526421 & 033 535 & Michael Kaufmann & e1526421@student.tuwien.ac.at\\
\end{tabular}
\end{table}

\end{center}
\vspace{1.0cm}

\end{titlepage}
\setcounter{page}{2}
{
  \hypersetup{linkcolor=black}
  \tableofcontents
	\vspace{2cm}
}

\section{Executive Summary}
Im folgenden Protokoll werden zwei Systeme, ein simples ohne Ausfallssicherungsmaßnahmen und eines mit redundanten Komponenten, hinsichtlich Verfügbarkeit, der Mean-Time-To-Failure (MTTF) und der Betriebskosten verglichen. Die redundante Variante kann naturgemäß bei der Verfügbarkeit und der MTTF viel bessere Ergebnisse erzielen, jedoch rentiert sie sich bei der simplen Betriebskostenrechnung erst ab relativ hoher Ausfallskosten des Systems im Vergleich zu den normalen Betriebskosten. 

\newpage
\section{Problemstellung}
Die Problemstellung lautete zwei Computersysteme (Abbildung \ref{fig:angabe_systeme}) in den Aspekten der Verfügbarkeit, Fehlertoleranz und Mehrkosten zu vergleichen. Dabei wird angenommen, dass die Systeme zum Erfüllen ihrer Funktion gemäß der Spezifikation mindestens ein funktionierendes Netzwerk, in jeder FTU mindestens einen funktionierenden Rechner (Node) und auch den Knoten mit der Uhr benötigt. Die Fehlerraten bzw. Reparaturraten sind: $\lambda_R$ bzw. $\mu_R$ für Rechner (Nodes),$\lambda_C$ bzw. $\mu_C$ für die Uhr und $\lambda_N$ bzw. $\mu_N$ für die Netzwerke. Folgende Werte wurden Vorgegeben: $\lambda_R = \frac{10^{-4}}{Std}$, $\lambda_C = \frac{10^{-7}}{Std}$, $\lambda_N = \frac{10^{-5}}{Std}$, $\mu_R = \mu_C = \mu_N = \frac{10^{-2}}{Std}$.
\begin{figure}[H]
		\centering
		\resizebox{.9\textwidth}{!}{
			\subfigure[Einfaches System]{
				\label{fig:angabe_simple_system}
				\includegraphics[height=5cm]{system_simpel.pdf}
			} 
			\subfigure[Redundantes System]{
				\label{fig:angabe_redundant_system}
				\includegraphics[height=5cm]{system_redundant.pdf}
			} 
		}
\caption{Schemata der Computersysteme} 
\label{fig:angabe_systeme}
\end{figure}

Weiters sollte die Availability des redundanten Systems mit der Availability eines anderen redundaten Systems (Abbildung \ref{fig:redundant_system_noclock}) verglichen werden.

\begin{figure}[H]
	\centering
		\includegraphics[height=5cm]{system_redundant_noclock.pdf}
	\caption{Schema Redundantes System ohne Clock}
	\label{fig:redundant_system_noclock}
\end{figure}

\newpage
\section{Lösungsmethode}
Zur Lösung dieser Problemstellung wurden die Systeme als Markov Graphen modelliert und mit der Computersoftware Sharpe die MTTF und die Availability berechnet. Zwischen der Berechnung der MTTF und der Availability musste der Graph neu modelliert werden, da für die MTTF nicht angenommen werden darf, dass das System aus dem Fehlerzustand zurück in einen funktionierenden Zustand kommen kann und für die Availability ist dies notwendig. Im folgenden Kapitel werden die zur Berechnung notwendigen Modelle beschrieben.

\section{Markovgraphen}
In diesem Kapitel werden die erstellten Modelle zur Berechnung der notwendigen Werte beschrieben. In den folgenden Graphen ist $0$ immer der Anfangszustand eines Systems. Zustände, die mit $F$ gekennzeichnet sind, sind Zustände in denen das System nicht ordnungsgemäß funktioniert. 

In allen Markovgraphen ist der Startzustand ($P(t=0):=1$) der Zustand 0. 
\subsection{Mean-Time-To-Failure (MTTF)}
Für die Berechnung der Mean-Time-To-Failure (MTTF) wird ein Graph benötigt,  der die einzelnen Zustände des Systems bis zu einem Totalausfall modelliert. Die Kanten geben dabei die Wahrscheinlichkeit eines Übergangs in diesen Status an.
\subsubsection{Einfache Variante}
In der einfachen Variante (Abbildung \ref{fig:angabe_simple_system}) fällt das System sofort komplett aus sobald ein Subsystem ausfällt. Dies macht den Graphen sehr einfach. Zur weiteren Vereinfachung könnte man die Fehlerzustände zu einem einzigen Zustand zusammenfassen, jedoch benötigt man die Fehlerzustände für die Berechnung der Availability getrennt.

\begin{figure}[H]
\centering
\begin{tikzpicture}[>=stealth',shorten >=1pt,->,auto,node distance=2.5cm,
                    semithick]
  \tikzstyle{every state}=[draw=black,text=black,scale=0.8]

  \node[state] (0)						{0};
  \node[state] (2)	[right of=0]		{$F2$};
  \node[state] (1)	[above of=2]		{$F1$};
  \node[state] (3)	[below of=2]		{$F3$};
  
  \path (0) edge node [pos=0.7]{$\lambda_C$} (1) 
  			edge node [sloped] {$3 \lambda_R$}(2)
  			edge node [pos=0.7] {$\lambda_N$}(3);
\end{tikzpicture}
\caption{Markov Graph für MTTF des einfachen Systems} 
\label{tikz:MTTF_simple}
\end{figure}

In der folgenden Tabelle sind die Zustände und die Bedeutung, wenn sich das System in diesem Zustand befindet beschrieben.
\begin{table}[H]
\centering
 \begin{tabular}{r|l}
 Zustand & Beschreibung\\
  \hline
  $0$  & System funktioniert\\
  $F1$ & Genau eine Node funktioniert nicht \\
  $F2$ & Netzwerk funktioniert nicht\\
  $F3$ & Clock funktioniert nicht\\
 \end{tabular}
	\caption{Zustandsbeschreibung des Markov Graph für MTTF des einfachen Systems} 
 \label{tab:zustaende_MTTF_simple}
 \end{table}

\newpage
Für die Berechnung in Sharpe wird nur die \textbf{Adjazenzliste} benötigt:
\begin{table}[H]
\centering
\begin{tabular}{r|l}
Zustand & Erreichbare Zustände[Kosten] \\ 
\hline 
0 & $F1[\lambda_C]$, $F2[3 \cdot\lambda_R]$, $F3[\lambda_N]$ \\ 
1 & $\emptyset$ \\ 
2 & $\emptyset$ \\ 
3 & $\emptyset$ \\ 
\end{tabular} 
\caption{Adjazenzliste des Markov Graph für MTTF des einfachen Systems} 
\end{table}

\pagebreak
\subsubsection{Redundante Variante}
In der redundanten Variante des Systems (Abbildung \ref{fig:angabe_redundant_system}) ist der Graph aufgrund der verschiedenen Ausfallsarten komplexer. Für jede unterschiedliche Ausfallsart mit unterschiedlichen Ausfalls/Reperaturkosten wird ein Zustand benötigt.

\begin{figure}[H]
\centering
\begin{tikzpicture}[>=stealth',shorten >=1pt,->,auto,node distance=3.5cm,
                    semithick]
  \tikzstyle{every state}=[draw=black,text=black,scale=0.7]
\pgfmathsetmacro{\shift}{0.3ex}

  \node[state] (0)						{0};
  \node[state] (1)	[right of=0]		{1};
  \node[state] (2)	[right of=1]		{2};
  \node[state] (3)	[right of=2]		{3};
  \node[state] (5)	[below of=0]		{5};
  \node[state] (6)	[right of=5]		{6};
  \node[state] (7)	[right of=6]		{7};
  \node[state] (8)	[right of=7]		{8};
  \node			(t) [right of=8]        {};
  \node[state] (F)	[above right of=t,node distance=1.75cm]		{$F$};
  
  %\draw[transform canvas={yshift=0.5ex,xshift=\shift/2},->] (0) --(1) node[above,midway] {$6\lambda_R$};
  %\draw[transform canvas={yshift=-0.5ex},->](1) -- (0) node[below,midway] {$\mu_R$};
  \path (0.north east)edge  node{ $6\lambda_R$} (1.north west)
  		(0.south west)edge  node[left]{ $2\lambda_N$} (5.north west)
  		(0.north)	edge [bend left=80,pos=0.25] node{$\lambda_C$} (F)
  		
  		(1.south west)edge  node{ $\mu_R$} (0.south east)
  		(1.north east)edge  node{ $4 \lambda_R$} (2.north west)
  		(1.south west)edge  node[left]{ $2\lambda_N$} (6.north west)
  		(1.north) edge [bend left=60,pos=0.25] node{$\lambda_R+\lambda_C$} (F)
  		
  		(2.south west)edge  node{ $\mu_R$} (1.south east)
  		(2.north east)edge  node{ $2 \lambda_R$} (3.north west)
  		(2.south west)edge  node[left]{ $2\lambda_N$} (7.north west)
  		(2.north)	edge [bend left=50,pos=0.35] node{$2  \lambda_R+\lambda_C$} (F)
  		
  		(3.south west)edge  node{ $\mu_R$} (2.south east)
  		(3.south west)edge  node[left]{ $2\lambda_N$} (8.north west)
  		(3)edge node[pos=0.25,sloped]{$3  \lambda_R+\lambda_C$} (F)
  		
  		(5.north east)edge  node{ $6\lambda_R$} (6.north west)
  		(5.north east)edge  node[right]{  $\mu_N$} (0.south east)
  		(5.south)	edge [bend right=80,pos=0.25] node{$\lambda_N+\lambda_C$} (F)
  		
  		(6.north east)edge  node{ $4\lambda_R$} (7.north west)
  		(6.north east)edge  node[right]{  $\mu_N$} (1.south east)
  		(6.south west) edge node{$\mu_R$} (5.south east)
  		(6.south)edge [below,bend right=60,pos=0.35] node{$\lambda_N+\lambda_R+\lambda_C$} (F)
  		
  		(7.north east)edge  node{ $2\lambda_R$} (8.north west)
  		(7.north east)edge  node[right]{  $\mu_N$} (2.south east)
  		(7.south west) edge node{$\mu_R$} (6.south east)
  		(7.south)edge [below,bend right=50,pos=0.35] node{$\lambda_N+ 2  \lambda_R+\lambda_C$} (F)
  		
  		(8.north east)edge  node[right]{  $\mu_N$} (3.south east)
  		(8.south west) edge node{$\mu_R$} (7.south east)
  		(8) edge node[below,pos=0.5,sloped]{$\lambda_N+ 3\lambda_R+\lambda_C$} (F)
  		;
  		
\end{tikzpicture}
\caption{Markov Graph für MTTF des redundanten Systems} 
\label{tikz:MTTF_redundant}
\end{figure}

In der folgenden Tabelle sind die Zustände und die Bedeutung, wenn sich das System in diesem Zustand befindet beschrieben.
\begin{table}[H]
	\centering
	 \begin{tabular}{r|l}
			 Nummer & Beschreibung\\
				\hline
				$0$ & Alles funktioniert\\
				$1$ & Es funktioniert bei einem FTU ein Rechner nicht \\
				$2$ & Es funktioniert bei zwei FTUs jeweils ein Rechner nicht  \\ 
				$3$ & Es funktioniert bei drei FTUs jeweils ein Rechner nicht   \\
				$5$ & Es funktioniert ein Netzwerk nicht\\
				$6$ & Es funktioniert ein Netzwerk und bei einem FTU ein Rechner nicht \\
				$7$ & Es funktioniert ein Netzwerk und bei zwei FTUs jeweils ein Rechner nicht  \\ 
				$8$ & Es funktioniert ein Netzwerk und bei drei FTUs jeweils ein Rechner nicht   \\
				$F$ & Fehlerzustand\\
	 \end{tabular}
	\caption{Zustandsbeschreibung des Markov Graph für MTTF des redundanten Systems} 
	\label{tab:zustaende_MTTF_redundant}
\end{table}

\newpage
Für die Berechnung in Sharpe wird nur die \textbf{Adjazenzliste} benötigt:
\begin{table}[H]
\centering
\begin{tabular}{r|llll}
Zusatand & \multicolumn{4}{c}{Erreichbare Zustände[Kosten]} \\ 
\hline 
$0$ &													& $1[6 \cdot \lambda_R]$,	& $5[2\cdot\lambda_N]$,	& $F[\lambda_C]$ \\ 
$1$ & $0[\mu_R]$,							& $2[4 \cdot\lambda_R]$,	& $6[2\cdot\lambda_N]$,	& $F[\lambda_R+\lambda_C]$ \\ 
$2$ & $1[\mu_R]$,							& $3[2 \cdot\lambda_R]$,	& $7[2\cdot\lambda_N]$,	& $F[2 \cdot \lambda_R+\lambda_C]$ \\ 
$3$ & $2[\mu_R]$,							&													& $8[2\cdot\lambda_N]$,	& $F[3 \cdot \lambda_R+\lambda_C]$ \\ 
$5$ & $0[\mu_N]$,							&													& $6[6\cdot\lambda_R]$,	& $F[\lambda_N+\lambda_C]$ \\ 
$6$ & $1[\mu_N]$,							& $5[\mu_R]$,							& $7[4\cdot\lambda_R]$,	& $F[\lambda_N+\lambda_R+\lambda_C]$ \\ 
$7$ & $2[\mu_N]$,							&	$6[\mu_R]$,							& $8[2\cdot\lambda_R]$,	& $F[\lambda_N+ 2 \cdot \lambda_R+\lambda_C]$ \\ 
$8$ & $3[\mu_N]$,							& $7[\mu_R]$,							&												& $F[\lambda_N+ 3\cdot\lambda_R+\lambda_C]$ \\ 
$F$ & $\emptyset$ \\ 
\end{tabular} 
\caption{Adjazenzliste des Markov Graph für MTTF des redundanten Systems}
\end{table}






\newpage
\subsection{Availability}
Zur Berechnung der Availability des Systems muss die Möglichkeit einer Reperatur nach einem Totalausfall in betracht gezogen werden. Um den Graphen so einfach wie möglich zu halten, nehmen wir an, dass sobald ein Totalausfall des Systems passiert keine weitere Komponente beschädigt werden kann, bis der Fehler behoben wurde. In diesem Kapitel werden die Erweiterungen der benötigten Graphen beschrieben.
\subsubsection{Einfache Variante}
In der einfachen Variante des Systems (Abbildung \ref{fig:angabe_simple_system}) müssen nur aus den Fehlerzuständen aus Abbildung \ref{tikz:MTTF_simple} Pfade zurück in den funktionierenden Zustand (0) hinzugefügt werden. 
\begin{figure}[H]
\centering
\begin{tikzpicture}[>=stealth',shorten >=1pt,->,auto,node distance=2.5cm,
                    semithick]
  \tikzstyle{every state}=[draw=black,text=black,scale=0.8]

  \node[state] (0)						{0};
  \node[state] (2)	[right of=0]		{$F2$};
  \node[state] (1)	[above of=0]		{$F1$};
  \node[state] (3)	[below of=0]		{$F3$};
  
  \path (0) edge[bend left] node {$\lambda_C$} (1) 
  		(0)	edge[bend left] node [sloped] {$3 \lambda_R$}(2)
  		(0)	edge[bend left] node {$\lambda_N$}(3)
		
		(1) edge[bend left] node {$\mu_C$} (0)
		(2) edge[bend left] node {$\mu_R$} (0)
		(3) edge[bend left] node {$\mu_N$} (0)
  		
  		;
\end{tikzpicture}
\caption{Markov Graph für Availability des einfachen Systems} 
\label{tikz:Availability_simple}
\end{figure}

Die Bedeutung der Zustände in Abbildung \ref{tikz:Availability_simple} blieb gleich zu Abbildung \ref{tikz:MTTF_simple} und kann in Tabelle \ref{tab:zustaende_MTTF_simple} abgelesen werden.\\
Für die Berechnung in Sharpe wird nur die \textbf{Adjazenzliste} benötigt:
\begin{table}[H]
\centering
\begin{tabular}{r|l}
Zusatand & Erreichbare Zustände[Kosten] \\ 
\hline 
$0$  & $F1[\lambda_C]$, $F2[3 \cdot\lambda_R]$, $F3[\lambda_N]$ \\ 
$F1$ & 0[$\mu_C$] \\ 
$F2$ & 0[$\mu_R$] \\ 
$F3$ & 0[$\mu_N$] \\ 
\end{tabular} 
\caption{Adjazenzliste des Markov Graphen für Availability des einfachen Systems} 
\end{table}

\newpage
\subsubsection{Redundante Variante}
Die redundante Variante (Abbildung \ref{fig:angabe_redundant_system}) muss um einen Zustand pro Übergang und pro Fehlerart (Netzwerkausfall, Clockausfall, FTU-Ausfall) in den Fehlerzustand und um jeweils eine Kante aus diesen Fehlerzuständen heraus erweitert werden. Der daraus entstandene Graph ist in Abbildung \ref{tikz:Availability_redundant} zu sehen.

\begin{figure}[H]
\centering
\begin{tikzpicture}[>=stealth',shorten >=1pt,->,auto,node distance=4.5cm,
                    semithick,scale=0.6]
  \tikzstyle{every state}=[draw=black,text=black,scale=0.8]
\pgfmathsetmacro{\shift}{0.3ex}
\pgfmathsetmacro{\outerNodesLeftRight}{1.7cm}
\pgfmathsetmacro{\outerNodesUpDown}{3cm}
  \node[state] (0)						{0};
  \node[state] (1)	[right of=0]		{1};
  \node[state] (2)	[right of=1]		{2};
  \node[state] (3)	[right of=2]		{3};
  \node[state] (5)	[below of=0]		{5};
  \node[state] (6)	[right of=5]		{6};
  \node[state] (7)	[right of=6]		{7};
  \node[state] (8)	[right of=7]		{8};
  \node[state] (F01)	[above left of=0,node distance=2cm]		{$F01$};
  
  \node (F1t)	[above of=1,node distance=\outerNodesUpDown]		{};
  \node[state] (F11)	[left of=F1t,node distance=\outerNodesLeftRight]		{$F11$};
  \node[state] (F12)	[right of=F1t,node distance=\outerNodesLeftRight]		{$F12$};
  
  \node (F2t)	[above of=2,node distance=\outerNodesUpDown]		{};
  \node[state] (F21)	[left of=F2t,node distance=\outerNodesLeftRight]		{$F21$};
  \node[state] (F22)	[right of=F2t,node distance=\outerNodesLeftRight]		{$F22$};
  
  \node (F3t)	[above of=3,node distance=\outerNodesUpDown]		{};
  \node[state] (F31)	[left of=F3t,node distance=\outerNodesLeftRight]		{$F31$};
  \node[state] (F32)	[right of=F3t,node distance=\outerNodesLeftRight]		{$F32$};
  
  \node (F5t)			[left of=5,node distance=2cm]		{};
  \node[state] (F51)	[above of=F5t,node distance=\outerNodesLeftRight]		{$F51$};
  \node[state] (F52)	[below of=F5t,node distance=\outerNodesLeftRight]		{$F52$};
  
  \node (F6t0)			[below of=6,node distance=\outerNodesUpDown+1.5cm]		{};
  \node (F6t)			[left of=F6t0,node distance=2cm]		{};
  \node[state] (F63)	[below of=6,node distance=\outerNodesUpDown]		{$F63$};
  \node[state] (F62)	[left of=F63,node distance=\outerNodesLeftRight*1.5]		{$F62$};
  \node[state] (F61)	[left of=F62,node distance=\outerNodesLeftRight*1.5]		{$F61$};
  
  \node (F7t0)			[below of=7,node distance=\outerNodesUpDown]		{};
   \node (F7t)			[right of=F7t0,node distance=1cm]		{};
  \node[state] (F72)	[below of=7,node distance=\outerNodesUpDown]		{$F72$};
  \node[state] (F71)	[left of=F72,node distance=\outerNodesLeftRight*1.5]		{$F71$};
  \node[state] (F73)	[right of=F72,node distance=\outerNodesLeftRight*1.5]		{$F73$};
  
  \node[state] (F81)	[below of=8,node distance=\outerNodesUpDown] {$F81$};
  \node[state] (F82)	[right of=F81,node distance=\outerNodesLeftRight*1.5]		{$F82$};
  \node[state] (F83)	[right of=8,node distance=\outerNodesLeftRight*1.5]				{$F83$};
  
  %\draw[transform canvas={yshift=0.5ex,xshift=\shift/2},->] (0) --(1) node[above,midway] {$6\lambda_R$};
  %\draw[transform canvas={yshift=-0.5ex},->](1) -- (0) node[below,midway] {$\mu_R$};
  \path (0.north east)edge  node{ $6\lambda_R$} (1.north west)
  		(0.south west)edge  node[left]{ $2\lambda_N$} (5.north west)
		(0.north) edge node[right]{$\lambda_C$} (F01.east)  		
  		
  		(1.south west)edge  node{ $\mu_R$} (0.south east)
  		(1.north east)edge  node{ $4 \lambda_R$} (2.north west)
  		(1.south west)edge  node[left]{ $2\lambda_N$} (6.north west)
		(1.135) edge node[sloped]{$\lambda_C$} (F11.225)    		
  		(1.north)	edge node[sloped,pos=0.6]{$\lambda_R$} (F12)
  			
  		(2.south west)edge  node{ $\mu_R$} (1.south east)
  		(2.north east)edge  node{ $2 \lambda_R$} (3.north west)
  		(2.south west)edge  node[left]{ $2\lambda_N$} (7.north west)
  		(2.north west)edge node[sloped,pos=0.5]{$\lambda_C$} (F21.south west)
  		(2) edge node[sloped,pos=0.6]{$2\lambda_R$} (F22)
  		  
  		(3.south west)edge  node{ $\mu_R$} (2.south east)
  		(3.south west)edge  node[left]{ $2\lambda_N$} (8.north west)
  		(3.north west) edge node[sloped,pos=0.5]{$\lambda_C$} (F31.south west)
  		(3)  edge node[sloped,pos=0.6]{$3\lambda_R$} (F32)
  		  
  		(5.north east)edge  node{ $6\lambda_R$} (6.north west)
  		(5.north east)edge  node[right]{  $\mu_N$} (0.south east)
		(5.north west) edge node[above,sloped]{$\lambda_C$} (F51.east)
		(5.west) edge node[above,sloped] {$\lambda_N$} (F52.north)
		    		
  		(6.north east)edge  node{ $4\lambda_R$} (7.north west)
  		(6.north east)edge  node[right]{  $\mu_N$} (1.south east)
  		(6.south west) edge node{$\mu_R$} (5.south east)
		(6.south west) edge node[above,sloped,pos=0.9]{$\lambda_C$} (F61.north)  
		(6.south west) edge node[above,sloped,pos=0.9]{$\lambda_N$} (F62.north west) 
		(6.south) edge node[above,sloped,pos=0.9]{$\lambda_R$} (F63.north west) 
		  		
  		(7.north east)edge  node{ $2\lambda_R$} (8.north west)
  		(7.north east)edge  node[right]{  $\mu_N$} (2.south east)
  		(7.south west) edge node{$\mu_R$} (6.south east)
  		(7.south west) edge node[above,sloped,pos=0.8]{$\lambda_C$} (F71.north)
  		(7.south west) edge node[below,sloped,pos=0.8]{$\lambda_N$} (F72.north west)
  		(7.south) edge node[below,sloped,pos=0.7]{$\lambda_R$} (F73.north west)
  		  
  		(8.north east)edge  node[right]{  $\mu_N$} (3.south east)
  		(8.south west) edge node{$\mu_R$} (7.south east)
  		(8.south west) edge node[above,sloped,pos=0.8]{$\lambda_C$} (F81.north west)
  		(8.south east) edge node[below,sloped,pos=0.8]{$\lambda_N$} (F82.north west)
  		(8.north east) edge node[above,sloped]{$3\lambda_R$} (F83.north west)	
  			
  		(F01.south) edge node [left]{$\mu_C$} (0.west)
  		
  		(F11.south) edge node [sloped,pos=0.2]{$\mu_C$} (1.north)
  		(F12.south east) edge node[sloped,pos=0.7]{$\mu_R$} (1.north east)
  		
  		(F21.south) edge node [sloped,pos=0.2]{$\mu_C$} (2.north)
  		(F22.south east) edge node[sloped,pos=0.7]{$\mu_R$} (2.north east)
  		
  		(F31.south) edge node [sloped,pos=0.2]{$\mu_C$} (3.north)
  		(F32.south east) edge node[sloped,pos=0.7]{$\mu_R$} (3.north east)
  		
  		(F51.south) edge node [below,sloped]{$\mu_C$} (5.west)
  		(F52.east) edge node [below,sloped]{$\mu_N$} (5.south)
  		
  		(F61.north east) edge node [below,sloped,pos=0.1]{$\mu_C$} (6.south)
  		(F62.north east) edge node [below,sloped,pos=0.1]{$\mu_N$} (6.south east)
  		(F63.north east) edge node [above,sloped]{$\mu_R$} (6.south east)
  		
  		(F71.north east) edge node [below,sloped,pos=0.2]{$\mu_C$} (7.south)
  		(F72.north east) edge node [above,sloped,pos=0.2]{$\mu_N$} (7.south east)
  		(F73.north) edge node [above,sloped,pos=0.3]{$\mu_R$} (7.south east)
  		
  		(F81.north east) edge node [below,sloped,pos=0.2]{$\mu_C$} (8.south east)
  		(F82.north) edge node [above,sloped,pos=0.2]{$\mu_N$} (8.east)
  		(F83.south west) edge node [above,sloped,pos=0.3]{$\mu_R$} (8.south east)
  		;
  		
\end{tikzpicture}
\caption{Markov Graph für Availability des redundanten Systems} 
\label{tikz:Availability_redundant}
\end{figure}
Die Bedeutung der Zustände in denen sich das System nicht in einem Fehlerzustand befindet, kann in Tabelle \ref{tab:zustaende_MTTF_redundant} abgelesen werden. Die Fehlerzustände haben eine der drei folgenden Bedeutungen, welche sich durch die Kanten beschreiben lässt: 
\begin{itemize}
	\item \textbf{Clock defekt}: wenn die Kanten zum Fehlerzustand mit $\lambda_C$ bzw. $\mu_C$ beschriftet sind, ist die Clock defekt was zu einem Totalausfall des Systems führt.
	\item \textbf{FTU defekt}: wenn die Kanten zum Fehlerzustand mit $x \cdot \lambda_R$ bzw. $\mu_R$ beschriftet sind, ist eine FTU defekt (beide Nodes in der FTU ausgefallen) was zu einem Totalausfall des Systems führt.
	\item \textbf{Netzwerk defekt}: wenn die Kanten zum Fehlerzustand mit $\lambda_N$ bzw. $\mu_N$ beschriftet sind, sind beide Netzwerke defekt was zu einem Totalausfall des Systems führt.
\end{itemize}
\newpage
Für die Berechnung in Sharpe wird nur die \textbf{Adjazenzliste} benötigt:
\begin{table}[H]
\centering
\begin{tabular}{r|llllll}
Zusatand & \multicolumn{6}{c}{Erreichbare Zustände[Kosten]} \\ 
\hline 
$0$   &													& $1[6 \cdot \lambda_R]$,	& $5[2\cdot\lambda_N]$,	& $F01[\lambda_C]$\\ 
$1$   & $0[\mu_R]$,							& $2[4 \cdot\lambda_R]$,	& $6[2\cdot\lambda_N]$,	& $F11[\lambda_C]$, & $F12[\lambda_R]$\\ 
$2$   & $1[\mu_R]$,							& $3[2 \cdot\lambda_R]$,	& $7[2\cdot\lambda_N]$,	& $F21[\lambda_C]$, & $F22[2 \cdot \lambda_R]$\\ 
$3$   & $2[\mu_R]$,							&													& $8[2\cdot\lambda_N]$,	& $F21[\lambda_C]$, & $F32[3 \cdot \lambda_R]$\\ 
$5$   & $0[\mu_N]$,							&													& $6[6\cdot\lambda_R]$,	& $F51[\lambda_C]$, & $F52[\lambda_N]$\\ 
$6$   & $1[\mu_N]$,							& $5[\mu_R]$,							& $7[4\cdot\lambda_R]$,	& $F61[\lambda_C]$, & $F62[\lambda_N]$, & $F63[\lambda_R]$ \\ 
$7$   & $2[\mu_N]$,							& $6[\mu_R]$,							& $8[2\cdot\lambda_R]$,	& $F71[\lambda_C]$, & $F72[\lambda_N]$, & $F73[2 \cdot \lambda_R]$ \\ 
$8$   & $3[\mu_N]$,							& $7[\mu_R]$,							&												& $F81[\lambda_C]$, & $F82[\lambda_N]$, & $F83[3\cdot\lambda_R]$ \\ 
$F01$ & $0[\mu_C]$\\
$F11$ & $1[\mu_C]$\\
$F12$ & $1[\mu_R]$\\
$F21$ & $2[\mu_C]$\\
$F22$ & $2[\mu_R]$\\
$F31$ & $3[\mu_C]$\\
$F32$ & $3[\mu_R]$\\
$F51$ & $5[\mu_C]$\\
$F52$ & $5[\mu_N]$\\
$F61$ & $6[\mu_C]$\\
$F62$ & $6[\mu_N]$\\
$F63$ & $6[\mu_R]$\\
$F71$ & $7[\mu_C]$\\
$F72$ & $7[\mu_N]$\\
$F73$ & $7[\mu_R]$\\
$F81$ & $8[\mu_C]$\\
$F82$ & $8[\mu_N]$\\
$F83$ & $8[\mu_R]$\\ 
\end{tabular} 
\caption{Adjazenzliste des Markov Graph für MTTF des redundanten Systems}
\end{table}


\newpage
\subsubsection{Redundante Variante ohne Clock}
In einem weiteren Punkt der Aufgabenstellung sollte auch die Availability zwischen 2 unterschiedlichen redundaten Systemen verglichen werden. Das 2. redundante System (Abbildung \ref{fig:redundant_system_noclock}) hat keine Clock sondern stattdessen eine weitere FTU.

Wir haben folgenden Graphen modelliert, um die Availability berechnen zu können.

\begin{figure}[H]
\centering
\begin{tikzpicture}[>=stealth',shorten >=1pt,->,auto,node distance=4.5cm,
                    semithick,scale=0.6]
  \tikzstyle{every state}=[draw=black,text=black,scale=0.75]
\pgfmathsetmacro{\shift}{0.3ex}
\pgfmathsetmacro{\outerNodesLeftRight}{1.7cm}
\pgfmathsetmacro{\outerNodesUpDown}{3cm}
  \node[state] (0)						{0};
  \node[state] (1)	[right of=0]		{1};
  \node[state] (2)	[right of=1]		{2};
  \node[state] (3)	[right of=2]		{3};
  \node[state] (4)	[right of=3]		{4};
  \node[state] (5)	[below of=0]		{5};
  \node[state] (6)	[right of=5]		{6};
  \node[state] (7)	[right of=6]		{7};
  \node[state] (8)	[right of=7]		{8};
  \node[state] (9)	[right of=8]		{9};
  
  \node[state] (F12)	[above of=1,node distance=\outerNodesUpDown]		{$F11$};
  
  \node[state] (F22)	[above of=2,node distance=\outerNodesUpDown]		{$F21$};
  
  \node[state] (F32)	[above of=3,node distance=\outerNodesUpDown]		{$F31$};
	
  \node[state] (F42)	[above of=4,node distance=\outerNodesUpDown]		{$F41$};
  
  \node[state] (F52)	[below of=5,node distance=\outerNodesUpDown]		{$F51$};
  
  \node[state] (F63)	[below of=6,node distance=\outerNodesUpDown]		{$F62$};
  \node[state] (F62)	[left of=F63,node distance=\outerNodesLeftRight*1.5]		{$F61$};
  
  \node[state] (F72)	[right of=F63,node distance=\outerNodesLeftRight*1.5]		{$F71$};
  \node[state] (F73)	[right of=F72,node distance=\outerNodesLeftRight*1.5]		{$F72$};
  
  \node[state] (F82)	[right of=F73,node distance=\outerNodesLeftRight*1.5]		{$F81$};
  \node[state] (F83)	[right of=F82,node distance=\outerNodesLeftRight*1.5]		{$F82$};
	
  \node[state] (F92)	[right of=F83,node distance=\outerNodesLeftRight*1.5]		{$F91$};
  \node[state] (F93)	[right of=F92,node distance=\outerNodesLeftRight*1.5]		{$F92$};
  
  %\draw[transform canvas={yshift=0.5ex,xshift=\shift/2},->] (0) --(1) node[above,midway] {$6\lambda_R$};
  %\draw[transform canvas={yshift=-0.5ex},->](1) -- (0) node[below,midway] {$\mu_R$};
  \path (0.north east)edge  node{ $8\lambda_R$} (1.north west)
  		(0.south west)edge  node[left]{ $2\lambda_N$} (5.north west)
  		
  		(1.south west)edge  node{$\mu_R$} (0.south east)
  		(1.north east)edge  node{$6 \lambda_R$} (2.north west)
  		(1.south west)edge  node[left]{ $2\lambda_N$} (6.north west)
  		(1.north west)	edge node[pos=0.6]{$\lambda_R$} (F12.south west)
  			
  		(2.south west)edge  node{$\mu_R$} (1.south east)
  		(2.north east)edge  node{$4 \lambda_R$} (3.north west)
  		(2.south west)edge  node[left]{$2\lambda_N$} (7.north west)
  		(2.north west) edge node[pos=0.6]{$2\lambda_R$} (F22.south west)
  		  
  		(3.south west)edge  node{$\mu_R$} (2.south east)
  		(3.south west)edge  node[left]{$2\lambda_N$} (8.north west)
  		(3.north east)edge  node{$2\lambda_R$} (4.north west)
  		(3.north west)  edge node[pos=0.6]{$3\lambda_R$} (F32.south west)
			
			(4.south west)edge  node{$\mu_R$} (3.south east)
  		(4.south west)edge  node[left]{$2\lambda_N$} (9.north west)
  		(4.north west)  edge node[pos=0.6]{$4\lambda_R$} (F42.south west)
  		  
  		(5.north east)edge  node{$8\lambda_R$} (6.north west)
  		(5.north east)edge  node[right]{$\mu_N$} (0.south east)
		  (5.south west) edge node[left]{$\lambda_N$} (F52.north west)
		    		
  		(6.north east)edge  node{$6\lambda_R$} (7.north west)
  		(6.north east)edge  node[right]{$\mu_N$} (1.south east)
  		(6.south west) edge node{$\mu_R$} (5.south east)
		  (6.south west) edge node[above,pos=0.9]{$\lambda_N$} (F62.north west) 
		  (6.south west) edge node[left,pos=0.9]{$\lambda_R$} (F63.north west) 
		  		
  		(7.north east)edge  node{ $4\lambda_R$} (8.north west)
  		(7.north east)edge  node[right]{  $\mu_N$} (2.south east)
  		(7.south west) edge node{$\mu_R$} (6.south east)
  		(7.south west) edge node[above left]{$\lambda_N$} (F72.north west)
  		(7.south west) edge node[left,pos=0.7]{$\lambda_R$} (F73.north west)
  		  
			(8.north east)edge  node{ $2\lambda_R$} (9.north west)
  		(8.north east)edge  node[right]{  $\mu_N$} (3.south east)
  		(8.south west) edge node{$\mu_R$} (7.south east)
  		(8.south west) edge node[above left]{$\lambda_N$} (F82.north west)
  		(8.south west) edge node[below]{$3\lambda_R$} (F83.north west)	
			
			
  		(9.north east)edge  node[right]{  $\mu_N$} (4.south east)
  		(9.south west) edge node{$\mu_R$} (8.south east)
  		(9.south west) edge node[above left]{$\lambda_N$} (F92.north west)
  		(9.south west) edge node[below]{$4\lambda_R$} (F93.north west)	
  			
  		(F12.south east) edge node{$\mu_R$} (1.north east)
  		
  		(F22.south east) edge node{$\mu_R$} (2.north east)
  		
  		(F32.south east) edge node{$\mu_R$} (3.north east)
			
			(F42.south east) edge node{$\mu_R$} (4.north east)
  		
  		(F52.north east) edge node [right]{$\mu_N$} (5.south east)
  		
  		(F62.north east) edge node [below,pos=0.2]{$\mu_N$} (6.south east)
  		(F63.north east) edge node [right]{$\mu_R$} (6.south east)
  		
  		(F72.north east) edge node [below right,pos=0.2]{$\mu_N$} (7.south east)
  		(F73.north) edge node [right]{$\mu_R$} (7.south east)
  		
  		(F82.north east) edge node [right,pos=0.2]{$\mu_N$} (8.south east)
  		(F83.north east) edge node [right]{$\mu_R$} (8.south east)
			
			(F92.north east) edge node [right,pos=0.2]{$\mu_N$} (9.south east)
  		(F93.north east) edge node [right]{$\mu_R$} (9.south east)
  		;
  		
\end{tikzpicture}
\caption{Markov Graph für Availability des redundanten Systems ohne Clock} 
\label{tikz:Availability_redundant_noclock}
\end{figure}


Die Bedeutung der Zustände ist weitgehend gleich zum ursprünglichen redundanten System.
\begin{table}[H]
	\centering
	 \begin{tabular}{r|l}
			 Nummer & Beschreibung\\
				\hline
				$0$ & Alles funktioniert\\
				$1$ & Es funktioniert bei einem FTU ein Rechner nicht \\
				$2$ & Es funktioniert bei zwei FTUs jeweils ein Rechner nicht  \\ 
				$3$ & Es funktioniert bei drei FTUs jeweils ein Rechner nicht   \\
				$4$ & Es funktioniert bei vier FTUs jeweils ein Rechner nicht   \\
				$5$ & Es funktioniert ein Netzwerk nicht\\
				$6$ & Es funktioniert ein Netzwerk und bei einem FTU ein Rechner nicht \\
				$7$ & Es funktioniert ein Netzwerk und bei zwei FTUs jeweils ein Rechner nicht  \\ 
				$8$ & Es funktioniert ein Netzwerk und bei drei FTUs jeweils ein Rechner nicht   \\
				$9$ & Es funktioniert ein Netzwerk und bei vier FTUs jeweils ein Rechner nicht   \\
				$F\#\#$ & Fehlerzustand\\
	 \end{tabular}
	\caption{Zustandsbeschreibung des Markov Graphen für Availability des redundanten Systems ohne Clock} 
	\label{tab:zustaende_Avail_redundant_noclock}
\end{table}
Die Bedeutung der Fehlerzustände ergibt sich wieder durch die Kanten des Fehlerzustandes:
\begin{itemize}
	\item \textbf{FTU defekt}: wenn die Kanten zum Fehlerzustand mit $x \cdot \lambda_R$ bzw. $\mu_R$ beschriftet sind, ist eine FTU defekt (beide Nodes in der FTU ausgefallen) was zu einem Totalausfall des Systems führt.
	\item \textbf{Netzwerk defekt}: wenn die Kanten zum Fehlerzustand mit $\lambda_N$ bzw. $\mu_N$ beschriftet sind, sind beide Netzwerke defekt was zu einem Totalausfall des Systems führt.
\end{itemize}
\newpage
Für die Berechnung in Sharpe wird nur die \textbf{Adjazenzliste} benötigt:
\begin{table}[H]
\centering
\begin{tabular}{r|lllll}
Zusatand & \multicolumn{5}{c}{Erreichbare Zustände[Kosten]} \\ 
\hline 
$0$   &													& $1[8  \lambda_R]$,	& $5[2\lambda_N]$\\ 
$1$   & $0[\mu_R]$,							& $2[6 \lambda_R]$,	& $6[2\lambda_N]$,	& $F11[\lambda_R]$ \\ 
$2$   & $1[\mu_R]$,							& $3[4 \lambda_R]$,	& $7[2\lambda_N]$,	& $F21[2  \lambda_R]$ \\ 
$3$   & $2[\mu_R]$,							& $4[2 \lambda_R]$,	& $8[2\lambda_N]$, & $F31[3  \lambda_R]$ \\ 
$4$   & $3[\mu_R]$,							&													& $9[2\lambda_N]$,	&	$F41[4  \lambda_R]$  \\
$5$   & $0[\mu_N]$,							&													& $6[8\lambda_R]$,	&	$F51[\lambda_N]$  \\ 
$6$   & $1[\mu_N]$,							& $5[\mu_R]$,							& $7[6\lambda_R]$,	& $F61[\lambda_N]$, & $F62[\lambda_R]$ \\ 
$7$   & $2[\mu_N]$,							& $6[\mu_R]$							& $8[4\lambda_R]$,	& $F71[\lambda_N]$, & $F72[2\lambda_R]$ \\ 
$8$   & $3[\mu_N]$,							& $7[\mu_R]$,							&	$9[2\lambda_R]$,	& $F81[\lambda_N]$, & $F82[3\lambda_R]$ \\ 
$9$   & $4[\mu_N]$,							& $8[\mu_R]$,							&										& $F91[\lambda_N]$, & $F92[4\lambda_R]$ \\ 
$F11$ & $1[\mu_R]$\\
$F21$ & $2[\mu_R]$\\
$F31$ & $3[\mu_R]$\\
$F51$ & $5[\mu_N]$\\
$F61$ & $6[\mu_N]$\\
$F62$ & $6[\mu_R]$\\
$F71$ & $7[\mu_N]$\\
$F72$ & $7[\mu_R]$\\
$F81$ & $8[\mu_N]$\\
$F82$ & $8[\mu_R]$\\ 
$F91$ & $9[\mu_N]$\\
$F92$ & $9[\mu_R]$\\ 
\end{tabular} 
\caption{Adjazenzliste des Markov Graph für MTTF des redundanten Systems ohne Clock}
\end{table}


\newpage
\section{Ergebnisse}
\subsection{MTTF}
\begin{figure}[H]
	\centering
		\includegraphics[width=15cm]{Bilder/BSP1_MTTF.pdf}
	\caption{Ausfallswahrscheinlichkeit}
	\label{fig:MTTF}
\end{figure}
In Abbildung \ref{fig:MTTF} kann man die Ausfallswahrscheinlichkeit über die Zeit hinweg für die beiden Markov Graphen (Abbildung \ref{tikz:MTTF_simple} \& \ref{tikz:MTTF_redundant}) sehen. Die Werte auf der X-Achse sind in Stunden aufgetragen. Die Y-Achse gibt an, mit welcher Wahrscheinlichkeit das System zum Zeitpunkt x im Fehlerzustand ist. Man erkennt, dass das redundante System eine viel höhere Lebensdauer aufweist. Die Mean-Time-To-Failure (MTTF) wurde auch mit Sharpe berechnet und beträgt für das redundante System etwa $162.296\; Stunden \approx 18,5\; Jahre$ und für das simple nur etwa $3.225\; Stunden \approx 134\; Tage$.
\subsection{Availability}
Die Availability der Systeme kann in der folgenden Tabelle abgelesen werden.
\begin{table}[H]
	\centering
		\begin{tabular}{r|l}
			System & Availability \\ 
			\hline 
			einfache Variante (Abbildung \ref{tikz:Availability_simple})   												& $96,9922697\%$ \\ 
			redundante Variante (Abbildung \ref{tikz:Availability_redundant})											& $99,9377125\%$ \\ 
			redundante Variante ohne Clock (Abbildung \ref{tikz:Availability_redundant_noclock})	& $99,9167448\%$ \\ 
		\end{tabular}
	\caption{Availability der Systeme}
	\label{tab:availability}
\end{table}
Wie man sehen kann, ist das redundante System mit Clock das Verlässlichste, dicht gefolgt vom System ohne Clock und danach kommt die einfache Variante, in der es sehr leicht zu Ausfällen kommen kann.

\section{Diskussion der Ergebnisse}
\subsection{Vergleich der MTTF von simplen System und redundanten System}
Die MTTF der beiden Systeme (Abbildung \ref{fig:angabe_systeme}) unterscheidet sich sehr stark, da das redundante System viel weniger Fehleranfällig ist als das simple System. Dies ist auch zu erwarten, da alle Komponenten bis auf die Clock doppelt ausgeführt wurden.
\subsection{Vergleich der Availability von simplen System und redundanten System}
Die Availability der beiden Systeme (Abbildung \ref{fig:angabe_systeme}) unterscheidet sich auch sehr voneinander. Auf ein Jahr gerechnet würde das simple System knapp $11\; Tage$ ausgefallen sein und das redundante System hingegen nur etwas weniger als $5,5\; Stunden$.
\subsection{Vergleich der Availability der beiden redundanten Systeme}
Die Availability der beiden redundanten Systeme (Abbildung \ref{fig:angabe_redundant_system} \& \ref{fig:redundant_system_noclock}) unterscheiden sich nicht so stark, wobei das System mit der Clock eine höhere Availability vorweist. Der Unterschied beläuft sich im Jahr auf etwa $1,8\; Stunden$.
\subsection{Vergleich der Betriebskosten von simplen System und redundanten System}
Die Betriebskosten der beiden Systeme (Abbildung \ref{fig:angabe_systeme}) sind folgendermaßen gegeben:
Dis redundante Variante kostet im Normalbetrieb etwa $2,5$ mal mehr als die simple Variante. Die Ausfallskosten sind bei beiden Systemen gleich.

Daraus ergeben sich 2 Funktionen abhängig von den Ausfallskosten $x$ ($a_{simp} \ldots$ Availability des simplen Systems, $a_{red} \ldots$ Availability des redundanten Systems aus Tabelle \ref{tab:availability}):
\begin{itemize}
	\item \textbf{Simple Variante:} $a_{simp}\cdot 1+(1-a_{simp})\cdot x$
	\item \textbf{Redundante Variante:} $a_{red}\cdot 2,5+(1-a_{red})\cdot x$
\end{itemize}
Die tatsächlichen Kosten werden hierbei vernachlässigt, da sie für einen allgemeinen Vergleich irrelevant sind. 
Die beiden Funktionen kann man in Abbildung \ref{fig:betriebskosten} betrachten. Auf der X Achse ist das Verhältnis von Ausfallskosten zu Betriebskosten aufgetragen. Auf der Y Achse sind die Kosten als Vielfaches der eigentlichen Betriebskosten aufgetragen. Der Break-Even-Point ist bei $51,9$-fachen Ausfallskosten im Vergleich zu den Betriebskosten der einfachen Variante gegeben.
\begin{figure}[H]
	\centering
	\begin{gnuplot}[terminal=pdf]
				set sample 1000
				set ylabel "Kosten"
				set xlabel "Ausfallskosten/Betriebskosten"
				simp(x)= 0.969922697*1+(1-0.969922697)*x
				red(x)=  0.999377125*2.5+(1-0.999377125)*x
				plot [0:100] simp(x) title 'Simple Variante', red(x) title 'Redundante Variante'
		\end{gnuplot}
		\caption{Betriebskosten der Systeme}
		\label{fig:betriebskosten}
	\end{figure}
Dies bedeutet, dass sich bei Ausfallskosten von mehr als $51,9$-fachen Betriebskosten das redundante System rentiert. Wird ein geringerer Wert angenommen, ist die simple Variante im Betrieb günstiger.
\section{Fazit}
Es wurde gezeigt, dass die redundante Variante eine viel längere Lebensdauer und eine viel höhere Availability aufweist als die simple Variante des Systems. Jedoch rentiert sie sich in der Betriebskostenberechnung nur bei hohen Ausfallskosten. Das Verwenden redundanter Systeme wird jedoch unerlässlich, wenn man hohe Anforderungen an die Downtime eines Systems hat. Eine der Vereinfachungen die in diesem Beispiel angenommen wurde, kann in der Realität gegen ein redundantes System sprechen: Wenn ein Knoten nicht vollständig ausfällt sondern fehlerhafte Outputs (byzantinische Fehler) produziert, so kann das System, je nach Funktion, ausfallen obwohl ein zweiter Node in der FTU verfügbar wäre welcher fehlerfrei funktioniert. Dies müsste in allen Markovgraphen mithilfe einer Assumtion Coverage abgedeckt werden.

\end{document}
