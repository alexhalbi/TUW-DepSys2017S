format 8 
factor on 

bind lambdaR 10^(-4)
bind lambdaC 10^(-7)
bind lambdaN 10^(-5)
bind muR 10^(-2)
bind muN 10^(-2)

markov SYS 
0 1 2*lambdaR
0 2 2*lambdaR
0 3 2*lambdaR
0 4 2*lambdaN
0 16   2*lambdaR
1 F   lambdaR
1 0   muR
1 5   2*lambdaN
1 8   2*lambdaR
1 10     2*lambdaR
1 20   2*lambdaR
2 F    lambdaR
2 0   muR
2 6   2*lambdaN
2 8   2*lambdaR
2 9     2*lambdaR
2 19     2*lambdaR
3 F    lambdaR
3 0   muR
3 7   2*lambdaN
3 9     2*lambdaR
3 10     2*lambdaR
3 18     2*lambdaR
4 F   lambdaN
4 0   muN
5 1   muN
5 F   lambdaR+lambdaN
5 12    2*lambdaR
5 14     2*lambdaR
5 23     2*lambdaR
6 2   muN
6 F     lambdaR+lambdaN
6 12      2*lambdaR
6 13      2*lambdaR
6 22     2*lambdaR
7 3   muN
7 F     lambdaR+lambdaN
7 14      2*lambdaR
7 13      2*lambdaR
7 21     2*lambdaR
8 2   muR
8 1   muR
8 F   2*lambdaR
8 11      2*lambdaR
8 12      2*lambdaN
8 24     2*lambdaR
9 2   muR
9 3   muR
9 F     2*lambdaR
9 11      2*lambdaR
9 13        2*lambdaN
9 25     2*lambdaR
10 3   muR
10 1   muR
10 F     2*lambdaR
10 11      2*lambdaR
10 14        2*lambdaN
10 26     2*lambdaR
11 8 muR
11 9 muR
11 10 muR
11 F   3*lambdaR+lambdaN
11 15        2*lambdaN
11 30   2*lambdaR
12 8 muN
12 F   2*lambdaR+lambdaN
12 15        2*lambdaR
12 5 muR
12 6 muR
13 9 muN
13 F     2*lambdaR+lambdaN
13 15        2*lambdaR
13 6 muR
13 7 muR
14 10 muN
14 F     2*lambdaR+lambdaN
14 15        2*lambdaR
14 5 muR
14 7 muR
15 F     3*lambdaR+lambdaN
15 11 muN
15 14 muR
15 13 muR
15 12 muR
15 31         2*lambdaR
17 16   muN
17 21   2*lambdaR
17 22     2*lambdaR
17 23     2*lambdaR
17 F   lambdaR+lambdaN
16 0   muR
16 17   2*lambdaN
16 18  2* lambdaR
16 19    2* lambdaR
16 20    2* lambdaR
16 F lambdaR
18 16   muR
18 3   muR
18 21   2*lambdaN
18 F   2*lambdaR
18 24       2*lambdaR
19 16   muR
19 2 muR
19 22     2*lambdaN
19 F     2*lambdaR
19 25       2*lambdaR
20 16   muR
20 23     2*lambdaN
20 F     2*lambdaR
20 26       2*lambdaR
20 1 muR
21 17   muR
21 7   muR
21 18   muN
21 F     2*lambdaR+lambdaN
21 27   2*lambdaR
22 17   muR
22 6   muR
22 19   muN
22 F       2*lambdaR+lambdaN
22 28   2*lambdaR
23 17   muR
23 5   muR
23 20   muN
23 F       2*lambdaR+lambdaN
23 29   2*lambdaR
24 18   muR
24 8   muR
24 27   2*lambdaN
24 30         2*lambdaR
24 F     3*lambdaR
25 19   muR
25 9   muR
25 28    2*lambdaN
25 30         2*lambdaR
25 F     3*lambdaR
26 10   muR
26 20   muR
26 29    2*lambdaN
26 30         2*lambdaR
26 F     3*lambdaR
27 24   muN
27 31         2*lambdaR
27 F     3*lambdaR+lambdaN
27 12 muR
27 21 muR
28 25     muN
28 31         2*lambdaR
28 F     3*lambdaR+lambdaN
28 13 muR
28 22 muR
29 26     muN
29 31         2*lambdaR
29 F     3*lambdaR+lambdaN
29 14 muR
29 23 muR
30 24   muR
30 25     muR
30 26     muR
30 11   muR
30 31         2*lambdaN
30 F     4*lambdaR
31 30      muN
31 15   muR
31 27      muR
31 28      muR
31 29      muR
31 F     4*lambdaR+lambdaN
end
* Starting Probabilities defined: 
0 1.0
end

echo **************************************************************************** 
echo *********  Outputs asked for the model: Simple_Clock ************** 


echo Input parameters values: lambdaR= 10^(-4) , lambdaC=10^(-7), lambdaN=10^(-5) 

* evaluation
cdf(SYS)
eval(SYS) 0 18000 180
expr value(60;SYS)
expr mean(SYS)
end
