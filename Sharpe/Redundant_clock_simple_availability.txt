format 8 
factor on 


bind lamR 10^(-4)
bind lamC 10^(-7)
bind lamN 10^(-5)
bind muR 10^(-2)
bind muN 10^(-2)
bind muC 10^(-2)

markov SYS
0 1   6*lamR
0 5   2*lamN
0 14 lamC
1 2   4*lamR
1 6     2*lamN
1 0 muR
1 34 lamC
1 33 lamR
2 3   2*lamR
2 7     2*lamN
2 1 muR
2 30 lamC
2 31 2*lamR
3 8     2*lamN
3 2 muR
3 29 3*lamR
3 28 lamC
5 6   6*lamR
5 0 muN
5 16 lamC
5 17 lamN
6 7 4*lamR
6 1 muN
6 5 muR
6 18 lamR
6 19 lamC
6 20 lamN
7 8     2*lamR
7 2 muN
7 6 muR
7 21 2*lamR
7 22 lamC
7 23 lamN
8 3 muN
8 7 muR
8 24 lamC
8 25 3*lamR
8 26 lamN
14 0 muC
16 5 muC
17 5 muN
18 6 muR
19 6 muC
20 6 muN
21 7 muR
22 7 muC
23 7 muC
24 8 muC
25 8 muR
26 8 muN
28 3 muC
29 3 muR
30 2 muC
31 2 muR
33 1 muR
34 1 muC
end
0 1.0
end

echo Availability:
expr 1-(prob(SYS,14)+prob(SYS,16)+prob(SYS,17)+prob(SYS,18)+prob(SYS,19)+prob(SYS,20)+prob(SYS,21)+prob(SYS,22)+prob(SYS,23)+prob(SYS,24)+prob(SYS,25)+prob(SYS,26)+prob(SYS,28)+prob(SYS,29)+prob(SYS,30)+prob(SYS,31)+prob(SYS,33)+prob(SYS,34))
end